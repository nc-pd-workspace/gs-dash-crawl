package gs.dash.crawl.entity.bd;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@NoArgsConstructor
@Table(name = "BD_CMM_SIDO_M")
public class CmmSidoM {

    @Id
    @Column
    private String sidoCd;

    @Column
    private String sidoNm;

    @Column
    private String etlDtm;

    @Builder
    public CmmSidoM(String sidoCd, String sidoNm, String etlDtm) {
        this.sidoCd = sidoCd;
        this.sidoNm = sidoNm;
        this.etlDtm = etlDtm;
    }
}
