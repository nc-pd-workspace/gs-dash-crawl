package gs.dash.crawl.entity.bd;

import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Table(name="BD_PRD_PRD_M")
public class PrdPrdM {
    @Id
    @Column
    private String prdCd;

    @Column
    private String prdNm;

    @Column
    private String prdStsCd;

    @Column
    private String supCd;

    @Column
    private String brandCd;

    @Column
    private String sectCd;

    @Column
    private String etlDtm;

    @Builder
    public PrdPrdM(String prdCd, String prdNm, String prdStsCd, String supCd,
                   String brandCd, String sectCd, String etlDtm){
        this.prdCd = prdCd;
        this.prdNm = prdNm;
        this.prdStsCd = prdStsCd;
        this.supCd = supCd;
        this.brandCd = brandCd;
        this.sectCd = sectCd;
        this.etlDtm = etlDtm;
    }
}
