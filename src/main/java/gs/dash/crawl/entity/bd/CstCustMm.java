package gs.dash.crawl.entity.bd;

import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@NoArgsConstructor
@Table(name = "BD_CST_CUST_MM")
public class CstCustMm {
    @Id
    @Column
    private String stdMm;

    @Column
    private String custKey;

    @Column
    private String gendrCd;

    @Column
    private String age5UnitCd;

    @Column
    private String ecCustGradeCd;

    @Column
    private String sigunguCd;

    @Column
    private String etlDtm;

    public CstCustMm( String stdMm, String custKey, String gendrCd,
                      String age5UnitCd, String ecCustGradeCd, String sigunguCd,
                      String etlDtm
    ){
        this.stdMm = stdMm;
        this.custKey = custKey;
        this.gendrCd = gendrCd;
        this.age5UnitCd = age5UnitCd;
        this.ecCustGradeCd = ecCustGradeCd;
        this.sigunguCd = sigunguCd;
        this.etlDtm = etlDtm;
    }
}
