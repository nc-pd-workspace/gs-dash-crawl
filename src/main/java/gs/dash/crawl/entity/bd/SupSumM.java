package gs.dash.crawl.entity.bd;

import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Table(name= "BD_SUP_SUM_M")
public class SupSumM {
    @Id
    @Column
    private String supCd;

    @Column
    private String supNm;

    @Column
    private String etlDtm;

    @Builder
    public SupSumM(String supCd, String supNm , String etlDtm){
        this.supCd = supCd;
        this.supNm = supNm;
        this.etlDtm = etlDtm;
    }
}
