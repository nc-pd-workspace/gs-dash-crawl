package gs.dash.crawl.entity.bd;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@Table(name = "BD_SECT_SECT_M")
public class SectSectM {
    @Id
    @Column(name="SECT_CD")
    private String sectCd;

    @Column(name="SECT_NM")
    private String sectNm;

    @Column(name="SECT_LVL_CD")
    private String sectLvlCd;

    @Column(name="SECT_L_CD")
    private String sectLCd;

    @Column(name="SECT_L_NM")
    private String sectLNm;

    @Column(name="SECT_M_CD")
    private String sectMCd;

    @Column(name="SECT_M_NM")
    private String sectMNm;

    @Column(name="SECT_S_CD")
    private String sectSCd;

    @Column(name="SECT_S_NM")
    private String sectSNm;

    @Column(name="ETL_DTM")
    private String etlDtm;

    @Builder
    public SectSectM(String sectCd, String sectNm, String sectLvlCd, String sectLCd,
                     String sectLNm, String sectMCd, String sectMNm, String sectSCd,
                     String sectSNm, String etlDtm){
            this.sectCd = sectCd;
            this.sectNm = sectNm;
            this.sectLvlCd = sectLvlCd;
            this.sectLCd = sectLCd;
            this.sectLNm = sectLNm;
            this.sectMCd = sectMCd;
            this.sectMNm = sectMNm;
            this.sectSCd = sectSCd;
            this.sectSNm = sectSNm;
            this.etlDtm = etlDtm;
    }
}
