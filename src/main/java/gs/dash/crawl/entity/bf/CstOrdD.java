package gs.dash.crawl.entity.bf;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Table(name = "BF_CST_ORD_D")
public class CstOrdD {
    @Id
    @Column
    private String stdDt;

    @Column
    private String custKey;

    @Column
    private String ordNo;

    @Column
    private String prdCd;

    @Column
    private String totOrdAmt;

    @Column
    private String totOrdQty;

    @Column
    private String etlDtm;

    @Builder
    public CstOrdD( String stdDt, String custKey, String ordNo, String prdCd,
                    String totOrdAmt, String totOrdQty, String etlDtm  ){
        this.stdDt = stdDt;
        this.custKey = custKey;
        this.ordNo = ordNo;
        this.prdCd = prdCd;
        this.totOrdAmt = totOrdAmt;
        this.totOrdQty = totOrdQty;
        this.etlDtm = etlDtm;
    }
}
