package gs.dash.crawl.code;

import lombok.Getter;

@Getter
public enum CollectStateCode implements BaseEnumCode<String> {
    COLLECT_PROGRESS("GS001", "CP", "Success"),
    COLLECT_FAILED("GS001", "CF", "Failed");

    private String groupCode;
    private String detailCode;
    private String detailCodeName;

    CollectStateCode(String groupCode, String detailCode, String detailCodeName) {
        this.groupCode = groupCode;
        this.detailCode = detailCode;
        this.detailCodeName = detailCodeName;
    }
}

