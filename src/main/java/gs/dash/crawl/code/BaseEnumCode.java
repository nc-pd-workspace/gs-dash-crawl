package gs.dash.crawl.code;

public interface BaseEnumCode<T> {

    <T> T getDetailCode();
}
