package gs.dash.crawl.code;

public enum LogMessage {
    INFO_FILE_CREATE_START("Create File"),
    INFO_FILE_CREATE_COMPLETE("Complete Creating File !!! ,,, {}"),

    INFO_FILE_DOWNLOAD_START("Start File Download from {} !"),
    INFO_FILE_DOWNLOAD_COMPLETE("Complete File Download.."),

    INFO_FILE_CHECK("Check File Size : {}"),

    ERROR_DEFAULT("UnExpected Error {}"),
    ERROR_PERMISSION_DENIED("Error!! - Permission Denied When creating Resource File in 'step -> reader', Plz Check mask ,,, {}"),
    ERROR_IOE_EXCEPTION("I/O Error When invoked ItemReader,{} , {}");

    public String msg;

    LogMessage(String msg){
        this.msg = msg;
    }

    public String MESSAGE(){
        return this.msg;
    }

}
