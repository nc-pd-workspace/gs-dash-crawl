package gs.dash.crawl.code;

public class CrawlConst {
    public final static String PRODUCT_TOTAL_JOB_NAME = "gsDashCrawlJob";

    public final static int FILE_RETRY_CNT = 3;
    public final static int DATA_STEP_CHUNK_SIZE_20 = 20;
    public final static int DATA_STEP_CHUNK_SIZE_100 = 100;
    public final static int DATA_STEP_CHUNK_SIZE_1000 = 1000;
    public final static int DATA_STEP_CHUNK_SIZE_5000 = 5000;
}
