package gs.dash.crawl.job.bf;

import gs.dash.crawl.code.CrawlConst;
import gs.dash.crawl.code.LogMessage;
import gs.dash.crawl.entity.bf.CstOrdD;
import gs.dash.crawl.job.listener.JobCompletionNotificationListener;
import gs.dash.crawl.repository.bf.BfCstOrdDRepository;
import gs.dash.crawl.utils.FileUrlDownload;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.validation.BindException;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Configuration
@EnableBatchProcessing
@RequiredArgsConstructor
@EnableScheduling
@Slf4j
@ComponentScan(basePackages = {"gs.dash.crawl.*"})
public class BfCstOrdDJobConfig {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    private final BfCstOrdDRepository bfCstOrdDRepository;

    @Value("${app.data.dir}")
    private String SAVE_PATH;

    @Value("${app.data.file.extension}")
    private String SAVE_FILE_EXE;

    @Value("${app.data.file.delimiter}")
    private String DELIMITER;

    private String SAVE_FILE_PATH;
    private String S3_URL;

    @PostConstruct
    public void init(){
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String  dateStr = format.format(date);

        //for local test
//        String profile = System.getProperty("spring.profiles.active");
//        if(profile.equals("local")){
//            dateStr = profile;
//        }else{
//            dateStr = format.format(date);
//        }

        SAVE_FILE_PATH = SAVE_PATH + "/BF_CST_ORD_D/"+dateStr;

        File dir = new File(SAVE_FILE_PATH);
        if(!dir.exists()){
            dir.mkdirs();
        }
    }

    @Bean(name = "bfCstOrdDjob")
    public Job bfCstOrdDjob(JobCompletionNotificationListener listener, Step bfCstOrdDStep) {
        return jobBuilderFactory.get("bfCstOrdDjob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(bfCstOrdDStep)
                .end()
                .build();
    }

    @Bean(name = "bfCstOrdDStep")
    public Step bfCstOrdDStep(ItemWriter<CstOrdD> bfCstOrdDWriter) throws Exception {
        return stepBuilderFactory.get("bfCstOrdDStep")
                .<CstOrdD, CstOrdD> chunk(CrawlConst.DATA_STEP_CHUNK_SIZE_5000)
                .reader(bfCstOrdDReader())
                .writer(bfCstOrdDWriter)
                .build();
    }

    @Bean
    public FlatFileItemReader<CstOrdD> bfCstOrdDReader() {
        String SAVE_FILE_NAME = "part-00000-BF_CST_ORD_D.csv";
        File file = new File(SAVE_FILE_PATH + "/"+SAVE_FILE_NAME);

        try{
            log.info(LogMessage.INFO_FILE_CREATE_START.MESSAGE());
            if(!file.exists()){
                file.createNewFile();
            }
            log.info(LogMessage.INFO_FILE_CREATE_COMPLETE.MESSAGE(),file.getAbsolutePath());
        }catch(PermissionDeniedDataAccessException pda){
            log.error(LogMessage.ERROR_PERMISSION_DENIED.MESSAGE(),file.getAbsolutePath());
        }catch(IOException ioe){
            log.error(LogMessage.ERROR_IOE_EXCEPTION.MESSAGE(),file.getAbsolutePath());
        }catch(Exception e){
            log.error(LogMessage.ERROR_DEFAULT.MESSAGE(),e.getMessage());
        }
        finally {
            log.info(LogMessage.INFO_FILE_DOWNLOAD_START.MESSAGE(),S3_URL);
            //            로컬 테스트 위한 임시 주석
//            FileUrlDownload.fileUrlReadAndDownloadS3(S3_URL, file.getName(), SAVE_FILE_PATH, CrawlConst.FILE_RETRY_CNT);
            log.info(LogMessage.INFO_FILE_DOWNLOAD_COMPLETE.MESSAGE());
        }
        Long fs = file.length();
        log.info(LogMessage.INFO_FILE_CHECK.MESSAGE(),fs);

        return Long.compare(fs,0L) > 0  ?
                new FlatFileItemReaderBuilder<CstOrdD>()
                        .linesToSkip(1)
                        .name("bfCstOrdDReader")
                        .resource(new FileSystemResource(file.getAbsolutePath()))  //S3 Repository // Error
                        .delimited()
                        .delimiter("\t")
                        .names(new String[] {"stdDt", "custKey", "ordNo", "prdCd",
                                             "totOrdAmt", "totOrdQty","etlDtm"})
                        .fieldSetMapper(new FieldSetMapper<CstOrdD>() {
                            @Override
                            public CstOrdD mapFieldSet(FieldSet fieldSet) throws BindException {
                                String stdDt = fieldSet.readString(0);
                                String custKey = fieldSet.readString(1);
                                String ordNo = fieldSet.readString(2);
                                String prdCd = fieldSet.readString(3);
                                String totOrdAmt = fieldSet.readString(4);
                                String totOrdQty = fieldSet.readString(5);
                                String etlDtm = fieldSet.readString(6);
                                return new CstOrdD(stdDt, custKey, ordNo, prdCd,
                                                   totOrdAmt, totOrdQty, etlDtm);
                            }
                        })
                        .build()
                : null ;

    }

    @Bean
    public ItemWriter<CstOrdD> bfCstOrdDWriter() {
        return ((List<? extends CstOrdD> list) ->{
                bfCstOrdDRepository.saveAll(list);
            }
        );
    }
}


