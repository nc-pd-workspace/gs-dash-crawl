package gs.dash.crawl.job.bd;

import gs.dash.crawl.code.CrawlConst;
import gs.dash.crawl.code.LogMessage;
import gs.dash.crawl.entity.bd.CstCustMm;
import gs.dash.crawl.job.listener.JobCompletionNotificationListener;
import gs.dash.crawl.repository.bd.BdCstCustMmRepository;
import gs.dash.crawl.utils.FileUrlDownload;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.validation.BindException;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Configuration
@EnableBatchProcessing
@RequiredArgsConstructor
@EnableScheduling
@Slf4j
@ComponentScan(basePackages = {"gs.dash.crawl.*"})
public class BdCstCustMmJobConfig {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    private final BdCstCustMmRepository bdCstCustMmRepository;

    @Value("${app.data.dir}")
    private String SAVE_PATH;

    @Value("${app.data.file.extension}")
    private String SAVE_FILE_EXE;

    @Value("${app.data.file.delimiter}")
    private String DELIMITER;

    private String SAVE_FILE_PATH;
    private String S3_URL;

    @PostConstruct
    public void init(){
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String  dateStr = format.format(date);

        //for local test
//        String profile = System.getProperty("spring.profiles.active");
//        if(profile.equals("local")){
//            dateStr = profile;
//        }else{
//            dateStr = format.format(date);
//        }

        SAVE_FILE_PATH = SAVE_PATH + "/BD_CST_CUST_MM/"+dateStr;

        File dir = new File(SAVE_FILE_PATH);
        if(!dir.exists()){
            dir.mkdirs();
        }
    }

    @Bean(name = "bdCstCustMmjob")
    public Job bdCstCustMmjob(JobCompletionNotificationListener listener, Step bdCstCustMmStep) {
        return jobBuilderFactory.get("bdCstCustMmjob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(bdCstCustMmStep)
                .end()
                .build();
    }

    @Bean(name = "bdCstCustMmStep")
    public Step bdCstCustMmStep(ItemWriter<CstCustMm> bdCstCustMmWriter) throws Exception {
        return stepBuilderFactory.get("bdCstCustMmStep")
                .<CstCustMm, CstCustMm> chunk(CrawlConst.DATA_STEP_CHUNK_SIZE_5000)
                .reader(bdCstCustMmReader())
                .writer(bdCstCustMmWriter)
                .build();
    }

    @Bean
    public FlatFileItemReader<CstCustMm> bdCstCustMmReader() {
        String SAVE_FILE_NAME = "part-00000-BD_CST_CUST_MM.csv";
        File file = new File(SAVE_FILE_PATH + "/"+SAVE_FILE_NAME);

        try{
            log.info(LogMessage.INFO_FILE_CREATE_START.MESSAGE());
            if(!file.exists()){
                file.createNewFile();
            }
            log.info(LogMessage.INFO_FILE_CREATE_COMPLETE.MESSAGE(),file.getAbsolutePath());
        }catch(PermissionDeniedDataAccessException pda){
            log.error(LogMessage.ERROR_PERMISSION_DENIED.MESSAGE(),file.getAbsolutePath());
        }catch(IOException ioe){
            log.error(LogMessage.ERROR_IOE_EXCEPTION.MESSAGE(),file.getAbsolutePath());
        }catch(Exception e){
            log.error(LogMessage.ERROR_DEFAULT.MESSAGE(),e.getMessage());
        }
        finally {
            log.info(LogMessage.INFO_FILE_DOWNLOAD_START.MESSAGE(),S3_URL);
            //            로컬 테스트 위한 임시 주석
//            FileUrlDownload.fileUrlReadAndDownloadS3(S3_URL, file.getName(), SAVE_FILE_PATH, CrawlConst.FILE_RETRY_CNT);
            log.info(LogMessage.INFO_FILE_DOWNLOAD_COMPLETE.MESSAGE());
        }
        Long fs = file.length();
        log.info(LogMessage.INFO_FILE_CHECK.MESSAGE(),fs);
        return Long.compare(fs,0L) > 0  ?
                new FlatFileItemReaderBuilder<CstCustMm>()
                        .linesToSkip(1)
                        .name("bdCstCustMmReader")
                        .resource(new FileSystemResource(file.getAbsolutePath()))  //S3 Repository // Error
                        .delimited()
                        .delimiter("\t")
                        .names(new String[] {"stdMm", "custKey", "gendrCd", "age5UnitCd",
                                             "ecCustGradeCd", "sigunguCd", "etlDtm"})
                        .fieldSetMapper(new FieldSetMapper<CstCustMm>() {
                            @Override
                            public CstCustMm mapFieldSet(FieldSet fieldSet) throws BindException {
                                String stdMm = fieldSet.readString(0);
                                String custKey = fieldSet.readString(1);
                                String gendrCd = fieldSet.readString(2);
                                String age5UnitCd = fieldSet.readString(3);
                                String ecCustGradeCd = fieldSet.readString(4);
                                String sigunguCd = fieldSet.readString(5);
                                String etlDtm = fieldSet.readString(6);
                                return new CstCustMm(stdMm, custKey, gendrCd, age5UnitCd,
                                                     ecCustGradeCd, sigunguCd, etlDtm);
                            }
                        })
                        .build()
                : null ;

    }

    @Bean
    public ItemWriter<CstCustMm> bdCstCustMmWriter() {
        return ((List<? extends CstCustMm> list) ->{
                bdCstCustMmRepository.saveAll(list);
            }
        );
    }
}
