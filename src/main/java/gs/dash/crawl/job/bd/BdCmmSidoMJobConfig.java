package gs.dash.crawl.job.bd;


import gs.dash.crawl.code.CrawlConst;
import gs.dash.crawl.code.LogMessage;
import gs.dash.crawl.entity.bd.CmmSidoM;
import gs.dash.crawl.job.listener.JobCompletionNotificationListener;
import gs.dash.crawl.repository.bd.BdCmmSidoMRepository;
import gs.dash.crawl.utils.FileUrlDownload;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.validation.BindException;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Configuration
@EnableBatchProcessing
@EnableScheduling
@RequiredArgsConstructor
@Slf4j
@ComponentScan(basePackages = {"gs.dash.crawl.*"})
public class BdCmmSidoMJobConfig {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    private final BdCmmSidoMRepository bdCmmSidoMRepository;

    @Value("${app.data.dir}")
    private String SAVE_PATH;

    @Value("${app.data.file.extension}")
    private String SAVE_FILE_EXE;

    @Value("${app.data.file.delimiter}")
    private String DELIMITER;

    private String SAVE_FILE_PATH;
    private String S3_URL;

    @PostConstruct
    public void init(){
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String dateStr = format.format(date);;

        SAVE_FILE_PATH = SAVE_PATH + "/BD_CMM_SIDO_M/"+dateStr;

        File dir = new File(SAVE_FILE_PATH);
        if(!dir.exists()){
            dir.mkdirs();
        }
    }

    @Bean(name = "bdCmmSidoMjob")
    public Job bdCmmSidoMjob(JobCompletionNotificationListener listener, Step bdCmmSidoMStep) {
        return jobBuilderFactory.get("bdCmmSidoMjob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(bdCmmSidoMStep)
                .end()
                .build();
    }

    @Bean(name = "bdCmmSidoMStep")
    public Step bdCmmSidoMStep(ItemWriter<CmmSidoM> bdCmmSidoMWriter) throws Exception {
        return stepBuilderFactory.get("bdCmmSidoMStep")
                .<CmmSidoM, CmmSidoM> chunk(CrawlConst.DATA_STEP_CHUNK_SIZE_100)
                .reader(bdCmmSidoMReader())
                .writer(bdCmmSidoMWriter)
                .build();
    }

    @Bean
    public FlatFileItemReader<CmmSidoM> bdCmmSidoMReader() {
        File file = new File(SAVE_FILE_PATH + "/part-00000-BD_CMM_SIDO_M.csv");

        try{
            log.info(LogMessage.INFO_FILE_CREATE_START.MESSAGE());
            if(!file.exists()){
                file.createNewFile();
            }
            log.info(LogMessage.INFO_FILE_CREATE_COMPLETE.MESSAGE(),file.getAbsolutePath());
        }catch(PermissionDeniedDataAccessException pda){
            log.error(LogMessage.ERROR_PERMISSION_DENIED.MESSAGE(),file.getAbsolutePath());
        }catch(IOException ioe){
            log.error(LogMessage.ERROR_IOE_EXCEPTION.MESSAGE(),file.getAbsolutePath());
        }catch(Exception e){
            log.error(LogMessage.ERROR_DEFAULT.MESSAGE(),e.getMessage());
        }
        finally {
            log.info(LogMessage.INFO_FILE_DOWNLOAD_START.MESSAGE(),S3_URL);
//            로컬 테스트 위한 임시 주석
//            FileUrlDownload.fileUrlReadAndDownloadS3(S3_URL, file.getName(), SAVE_FILE_PATH, CrawlConst.FILE_RETRY_CNT);
            log.info(LogMessage.INFO_FILE_DOWNLOAD_COMPLETE.MESSAGE());
        }

        Long fs = file.length();
        log.info(LogMessage.INFO_FILE_CHECK.MESSAGE(),fs);
        return Long.compare(fs,0L) > 0  ?
                new FlatFileItemReaderBuilder<CmmSidoM>()
                        .linesToSkip(1)
                        .name("bdCmmSidoMReader")
                        .resource(new FileSystemResource(file.getAbsolutePath()))  //S3 Repository // Error
                        .delimited()
                        .delimiter("\t")
                        .names(new String[] {"sidoCd", "sidoNm", "etlDtm"})
                        .fieldSetMapper(new FieldSetMapper<CmmSidoM>() {
                            @Override
                            public CmmSidoM mapFieldSet(FieldSet fieldSet) throws BindException {
                                String sidoCd = fieldSet.readString(0);
                                String sidoNm = fieldSet.readString(1);
                                String etlDtm = fieldSet.readString(2);
                                return new CmmSidoM(sidoCd,sidoNm, etlDtm);
                            }
                        })
                        .build()
                : null ;

    }

    @Bean
    public ItemWriter<CmmSidoM> bdCmmSidoMWriter() {
        return ((List<? extends CmmSidoM> list) ->{
                bdCmmSidoMRepository.saveAll(list);
            }
        );
    }

}