package gs.dash.crawl.job.bd;

import gs.dash.crawl.code.CrawlConst;
import gs.dash.crawl.code.LogMessage;
import gs.dash.crawl.entity.bd.PrdBrandM;
import gs.dash.crawl.job.listener.JobCompletionNotificationListener;
import gs.dash.crawl.repository.bd.BdPrdBrandMRepository;
import gs.dash.crawl.utils.FileUrlDownload;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.validation.BindException;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

//@Configuration
//@EnableBatchProcessing
@RequiredArgsConstructor
@EnableScheduling
@Slf4j
@ComponentScan(basePackages = {"gs.dash.crawl.*"})
public class BdPrdBrandMJobConfig {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    private final BdPrdBrandMRepository bdPrdBrandMRepository;

    @Value("${app.data.dir}")
    private String SAVE_PATH;

    @Value("${app.data.file.extension}")
    private String SAVE_FILE_EXE;

    @Value("${app.data.file.delimiter}")
    private String DELIMITER;

    private String SAVE_FILE_PATH;
    private String S3_URL;

    @PostConstruct
    public void init(){
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String  dateStr = format.format(date);

        //for local test
//        String profile = System.getProperty("spring.profiles.active");
//        if(profile.equals("local")){
//            dateStr = profile;
//        }else{
//            dateStr = format.format(date);
//        }

        SAVE_FILE_PATH = SAVE_PATH + "/BD_PRD_BRAND_M/"+dateStr;

        File dir = new File(SAVE_FILE_PATH);
        if(!dir.exists()){
            dir.mkdirs();
        }
    }

    @Bean(name = "bdPrdBrandMjob")
    public Job bdPrdBrandMjob(JobCompletionNotificationListener listener, Step bdPrdBrandMStep) {
        return jobBuilderFactory.get("bdPrdBrandMjob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(bdPrdBrandMStep)
                .end()
                .build();
    }

    @Bean(name = "bdPrdBrandMStep")
    public Step bdPrdBrandMStep(ItemWriter<PrdBrandM> bdPrdBrandMWriter) throws Exception {
        return stepBuilderFactory.get("bdPrdBrandMStep")
                .<PrdBrandM, PrdBrandM> chunk(CrawlConst.DATA_STEP_CHUNK_SIZE_5000)
                .reader(bdPrdBrandMReader())
                .writer(bdPrdBrandMWriter)
                .build();
    }

    @Bean
    public FlatFileItemReader<PrdBrandM> bdPrdBrandMReader() {
        String SAVE_FILE_NAME = "part-00000-BD_PRD_BRAND_M.csv";
        File file = new File(SAVE_FILE_PATH + "/" + SAVE_FILE_NAME);

        try{
            log.info(LogMessage.INFO_FILE_CREATE_START.MESSAGE());
            if(!file.exists()){
                file.createNewFile();
            }
            log.info(LogMessage.INFO_FILE_CREATE_COMPLETE.MESSAGE(),file.getAbsolutePath());
        }catch(PermissionDeniedDataAccessException pda){
            log.error(LogMessage.ERROR_PERMISSION_DENIED.MESSAGE(),file.getAbsolutePath());
        }catch(IOException ioe){
            log.error(LogMessage.ERROR_IOE_EXCEPTION.MESSAGE(),file.getAbsolutePath());
        }catch(Exception e){
            log.error(LogMessage.ERROR_DEFAULT.MESSAGE(),e.getMessage());
        }
        finally {
            log.info(LogMessage.INFO_FILE_DOWNLOAD_START.MESSAGE(),S3_URL);
            //            로컬 테스트 위한 임시 주석
//            FileUrlDownload.fileUrlReadAndDownloadS3(S3_URL, file.getName(), SAVE_FILE_PATH, CrawlConst.FILE_RETRY_CNT);
            log.info(LogMessage.INFO_FILE_DOWNLOAD_COMPLETE.MESSAGE());
        }
        Long fs = file.length();
        log.info(LogMessage.INFO_FILE_CHECK.MESSAGE(),fs);

        return Long.compare(fs,0L) > 0 ?
                new FlatFileItemReaderBuilder<PrdBrandM>()
                        .linesToSkip(1)
                        .name("bdPrdBrandMReader")
                        .resource(new FileSystemResource(file.getAbsolutePath()))  //S3 Repository // Error
                        .delimited()
                        .delimiter("\t")
                        .names(new String[] {"brandCd", "brandNm", "etlDtm"})
                        .fieldSetMapper(new FieldSetMapper<PrdBrandM>() {
                            @Override
                            public PrdBrandM mapFieldSet(FieldSet fieldSet) throws BindException {
                                String brandCd = fieldSet.readString(0);
                                String brandNm = fieldSet.readString(1);
                                String etlDtm = fieldSet.readString(2);
                                return new PrdBrandM(brandCd, brandNm, etlDtm);
                            }
                        })
                        .build()
                : null;
    }

    @Bean
    public ItemWriter<PrdBrandM> bdPrdBrandMWriter() {
        return ((List<? extends PrdBrandM> list) ->{
                bdPrdBrandMRepository.saveAll(list);
            }
        );
    }
}
