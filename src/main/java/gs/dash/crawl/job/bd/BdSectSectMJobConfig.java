package gs.dash.crawl.job.bd;

import gs.dash.crawl.code.CrawlConst;
import gs.dash.crawl.code.LogMessage;
import gs.dash.crawl.entity.bd.SectSectM;
import gs.dash.crawl.job.listener.JobCompletionNotificationListener;
import gs.dash.crawl.repository.bd.BdSectSectMRepository;
import gs.dash.crawl.utils.FileUrlDownload;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.validation.BindException;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Configuration
@EnableBatchProcessing
@RequiredArgsConstructor
@EnableScheduling
@Slf4j
@ComponentScan(basePackages = {"gs.dash.crawl.*"})
public class BdSectSectMJobConfig {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    private final BdSectSectMRepository bdSectSectMRepository;

    @Value("${app.data.dir}")
    private String SAVE_PATH;

    @Value("${app.data.file.extension}")
    private String SAVE_FILE_EXE;

    @Value("${app.data.file.delimiter}")
    private String DELIMITER;

    private String SAVE_FILE_PATH;
    private String S3_URL;

    @PostConstruct
    public void init(){
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String  dateStr = format.format(date);

        //for local test
//        String profile = System.getProperty("spring.profiles.active");
//        if(profile.equals("local")){
//            dateStr = profile;
//        }else{
//            dateStr = format.format(date);
//        }

        SAVE_FILE_PATH = SAVE_PATH + "/BD_SECT_SECT_M/"+dateStr;

        File dir = new File(SAVE_FILE_PATH);
        if(!dir.exists()){
            dir.mkdirs();
        }
    }

    @Bean(name = "bdSectSectMJob")
    public Job bdSectSectMJob(JobCompletionNotificationListener listener, Step bdSectSectMStep) {
        return jobBuilderFactory.get("bdSectSectMJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(bdSectSectMStep)
                .end()
                .build();
    }

    @Bean(name = "bdSectSectMStep")
    public Step bdSectSectMStep(ItemWriter<SectSectM> bdSectSectMWriter) throws Exception {
        return stepBuilderFactory.get("bdSectSectMStep")
                .<SectSectM, SectSectM> chunk(CrawlConst.DATA_STEP_CHUNK_SIZE_5000)
                .reader(bdSectSectMReader())
                .writer(bdSectSectMWriter)
                .build();
    }

    @Bean
    public FlatFileItemReader<SectSectM> bdSectSectMReader() {
        String SAVE_FILE_NAME = "part-00000-BD_SECT_SECT_M.csv";
        File file = new File(SAVE_FILE_PATH + "/"+SAVE_FILE_NAME);

        try{
            log.info(LogMessage.INFO_FILE_CREATE_START.MESSAGE());
            if(!file.exists()){
                file.createNewFile();
            }
            log.info(LogMessage.INFO_FILE_CREATE_COMPLETE.MESSAGE(),file.getAbsolutePath());
        }catch(PermissionDeniedDataAccessException pda){
            log.error(LogMessage.ERROR_PERMISSION_DENIED.MESSAGE(),file.getAbsolutePath());
        }catch(IOException ioe){
            log.error(LogMessage.ERROR_IOE_EXCEPTION.MESSAGE(),file.getAbsolutePath());
        }catch(Exception e){
            log.error(LogMessage.ERROR_DEFAULT.MESSAGE(),e.getMessage());
        }
        finally {
            log.info(LogMessage.INFO_FILE_DOWNLOAD_START.MESSAGE(),S3_URL);
            //            로컬 테스트 위한 임시 주석
//            FileUrlDownload.fileUrlReadAndDownloadS3(S3_URL, file.getName(), SAVE_FILE_PATH, CrawlConst.FILE_RETRY_CNT);
            log.info(LogMessage.INFO_FILE_DOWNLOAD_COMPLETE.MESSAGE());
        }
        Long fs = file.length();
        log.info(LogMessage.INFO_FILE_CHECK.MESSAGE(),fs);
        return Long.compare(fs,0L) > 0  ?
                new FlatFileItemReaderBuilder<SectSectM>()
                        .linesToSkip(1)
                        .name("bdSectSectMReader")
                        .resource(new FileSystemResource(file.getAbsolutePath()))  //S3 Repository // Error
                        .delimited()
                        .delimiter("\t")
                        .names(new String[] {"sectCd","sectNm","sectLvlCd","sectLCd"
                                            ,"sectLNm","sectMCd","sectMNm","sectSCd"
                                            ,"sectSNm","etlDtm"})
                        .fieldSetMapper(new FieldSetMapper<SectSectM>() {
                            @Override
                            public SectSectM mapFieldSet(FieldSet fieldSet) throws BindException {
                                String sectCd = fieldSet.readString(0);
                                String sectNm = fieldSet.readString(1);
                                String sectLvlCd = fieldSet.readString(2);
                                String sectLCd = fieldSet.readString(3);
                                String sectLNm = fieldSet.readString(4);
                                String sectMCd = fieldSet.readString(5);
                                String sectMNm = fieldSet.readString(6);
                                String sectSCd = fieldSet.readString(7);
                                String sectSNm = fieldSet.readString(8);
                                String etlDtm = fieldSet.readString(9);

                                return new  SectSectM(sectCd , sectNm , sectLvlCd , sectLCd
                                                    , sectLNm , sectMCd , sectMNm , sectSCd
                                                    , sectSNm , etlDtm);
                            }
                        })
                        .build()
                : null ;

    }

    @Bean
    public ItemWriter<SectSectM> bdSectSectMWriter() {
        return ((List<? extends SectSectM> sectList) ->{
                bdSectSectMRepository.saveAll(sectList);
            }
        );
    }
}
