package gs.dash.crawl.converter;

import gs.dash.crawl.code.BaseEnumCode;

import javax.persistence.AttributeConverter;
import java.util.Arrays;
import javax.persistence.AttributeConverter;

public abstract class AbstractBaseEnumConverter<X extends Enum<X> & BaseEnumCode<Y>, Y> implements
        AttributeConverter<X, Y> {

    protected abstract X[] getDetailCodeList();

    @Override
    public Y convertToDatabaseColumn(X attribute) {
        return attribute.getDetailCode();
    }

    @Override
    public X convertToEntityAttribute(Y dbData) {
        return Arrays.stream(getDetailCodeList())
                .filter(e -> e.getDetailCode().equals(dbData))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format("Unsupported type for %s.", dbData)));
    }
}
