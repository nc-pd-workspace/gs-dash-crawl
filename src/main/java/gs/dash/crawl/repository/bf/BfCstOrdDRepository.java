package gs.dash.crawl.repository.bf;

import gs.dash.crawl.entity.bf.CstOrdD;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BfCstOrdDRepository extends JpaRepository<CstOrdD,Long> {
}
