package gs.dash.crawl.repository.bd;

import gs.dash.crawl.entity.bd.CmmSidoM;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BdCmmSidoMRepository extends JpaRepository<CmmSidoM, Long> {

}
