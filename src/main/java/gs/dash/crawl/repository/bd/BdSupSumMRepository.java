package gs.dash.crawl.repository.bd;

import gs.dash.crawl.entity.bd.SupSumM;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BdSupSumMRepository extends JpaRepository<SupSumM,Long> {
}
