package gs.dash.crawl.repository.bd;

import gs.dash.crawl.entity.bd.PrdPrdM;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BdPrdPrdMRepository extends JpaRepository<PrdPrdM,Long> {
}
