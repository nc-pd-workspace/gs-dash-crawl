package gs.dash.crawl.repository.bd;

import gs.dash.crawl.entity.bd.SectSectM;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BdSectSectMRepository extends JpaRepository<SectSectM,Long> {
}
