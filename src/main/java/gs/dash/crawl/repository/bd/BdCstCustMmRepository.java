package gs.dash.crawl.repository.bd;

import gs.dash.crawl.entity.bd.CstCustMm;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BdCstCustMmRepository extends JpaRepository<CstCustMm,Long> {
}
