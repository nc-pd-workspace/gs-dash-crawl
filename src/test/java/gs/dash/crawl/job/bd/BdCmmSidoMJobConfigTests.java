package gs.dash.crawl.job.bd;

import gs.dash.crawl.repository.bd.BdCmmSidoMRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.GZIPInputStream;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BdCmmSidoMJobConfigTests {
    @Autowired
    BdCmmSidoMRepository bdCmmSidoMRepository;

    private String SAVE_PATH = "D:/data/gsshop/";

    @Test
    @Rollback(value=false)
    public void 압축해제_테스트() throws Exception{
        //압축 해제
        String path = SAVE_PATH + "BD_CMM_SIDO_M";
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String dateStr = format.format(date);

        String SAVE_FILE_PATH = path + "/"+dateStr;

        File dir = new File(SAVE_FILE_PATH);

        File []fileList = dir.listFiles();

        for(File zipFile : fileList) {
            GZIPInputStream gin = new GZIPInputStream(new FileInputStream(zipFile));
            FileOutputStream fos = null;
            try {
                File outFile = new File(zipFile.getParent(), zipFile.getName().replaceAll("\\.gz$", ""));
                fos = new FileOutputStream(outFile);
                byte[] buf = new byte[100000];
                int len;
                while ((len = gin.read(buf)) > 0) {
                    fos.write(buf, 0, len);
                }
                fos.close();
            } finally {
                if (gin != null) {
                    gin.close();
                }
                if (fos != null) {
                    fos.close();
                }
            }
        }
    }
}
